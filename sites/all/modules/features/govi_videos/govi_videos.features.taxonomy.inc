<?php
/**
 * @file
 * govi_videos.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function govi_videos_taxonomy_default_vocabularies() {
  return array(
    'videos_clasificacion' => array(
      'name' => 'Videos Clasificación',
      'machine_name' => 'videos_clasificacion',
      'description' => 'Clasificacion de videos de la entidad',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
