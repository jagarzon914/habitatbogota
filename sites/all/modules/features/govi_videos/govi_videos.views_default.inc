<?php
/**
 * @file
 * govi_videos.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function govi_videos_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'videos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'videos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Gestor de Videos';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '16';
  $handler->display->display_options['style_plugin'] = 'responsive_grid';
  $handler->display->display_options['style_options']['columns'] = '2';
  $handler->display->display_options['style_options']['default_classes'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Pie de página: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  /* Campo: Contenido: Video */
  $handler->display->display_options['fields']['field_video']['id'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['table'] = 'field_data_field_video';
  $handler->display->display_options['fields']['field_video']['field'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['label'] = '';
  $handler->display->display_options['fields']['field_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video']['click_sort_column'] = 'input';
  $handler->display->display_options['fields']['field_video']['type'] = 'youtube_thumbnail';
  $handler->display->display_options['fields']['field_video']['settings'] = array(
    'image_style' => '',
    'image_link' => 'colorbox',
    'colorbox' => array(
      'parameters' => array(
        'width' => '640',
        'height' => '480',
        'autoplay' => 1,
        'loop' => 0,
        'showinfo' => 0,
        'controls' => 0,
        'autohide' => 0,
        'iv_load_policy' => 0,
      ),
      'gallery' => 0,
    ),
  );
  /* Campo: Contenido: Título */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
  /* Campo: Contenido: Descripcion */
  $handler->display->display_options['fields']['field_video_descripcion']['id'] = 'field_video_descripcion';
  $handler->display->display_options['fields']['field_video_descripcion']['table'] = 'field_data_field_video_descripcion';
  $handler->display->display_options['fields']['field_video_descripcion']['field'] = 'field_video_descripcion';
  $handler->display->display_options['fields']['field_video_descripcion']['label'] = '';
  $handler->display->display_options['fields']['field_video_descripcion']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_descripcion']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_descripcion']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_video_descripcion']['settings'] = array(
    'trim_length' => '150',
  );
  /* Campo: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '	<div class="container-top">
		<div class="video-title">[title]</div>                
                <div class="video-info">
                      <div class="body-value">
                            [field_video_descripcion] 
                       </div>
                 </div>
	</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Contenido: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Contenido: Publicado */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Contenido: Tipo */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Campo: Contenido: Video */
  $handler->display->display_options['fields']['field_video']['id'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['table'] = 'field_data_field_video';
  $handler->display->display_options['fields']['field_video']['field'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['label'] = '';
  $handler->display->display_options['fields']['field_video']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_video']['alter']['text'] = '[field_video-video_id]';
  $handler->display->display_options['fields']['field_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video']['click_sort_column'] = 'input';
  $handler->display->display_options['fields']['field_video']['type'] = 'youtube_url';
  $handler->display->display_options['fields']['field_video']['settings'] = array(
    'link' => 1,
  );
  /* Campo: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = '';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<a href="http://www.youtube.com/v/[field_video]?fs=1&amp;width=640&amp;height=480&amp;hl=en_US1&amp;iframe=true&amp;&autoplay=1&amp;rel=0" class="colorbox-load">
<img src="http://img.youtube.com/vi/[field_video]/0.jpg">
</a>';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  /* Campo: Contenido: Título */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
  /* Campo: Contenido: Descripcion */
  $handler->display->display_options['fields']['field_video_descripcion']['id'] = 'field_video_descripcion';
  $handler->display->display_options['fields']['field_video_descripcion']['table'] = 'field_data_field_video_descripcion';
  $handler->display->display_options['fields']['field_video_descripcion']['field'] = 'field_video_descripcion';
  $handler->display->display_options['fields']['field_video_descripcion']['label'] = '';
  $handler->display->display_options['fields']['field_video_descripcion']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_descripcion']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_descripcion']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_video_descripcion']['settings'] = array(
    'trim_length' => '150',
  );
  /* Campo: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '	<div class="container-top">
		<div class="video-title">[title]</div>                
                <div class="video-info">
                      <div class="body-value">
                            [field_video_descripcion] 
                       </div>
                 </div>
	</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['path'] = 'videos';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_videos');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Pie de página: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<a href="/videos">Ver más videos</a>';
  $handler->display->display_options['footer']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Campo: Contenido: Video */
  $handler->display->display_options['fields']['field_video']['id'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['table'] = 'field_data_field_video';
  $handler->display->display_options['fields']['field_video']['field'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['label'] = '';
  $handler->display->display_options['fields']['field_video']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_video']['alter']['text'] = '[field_video-video_id]';
  $handler->display->display_options['fields']['field_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_video']['click_sort_column'] = 'input';
  $handler->display->display_options['fields']['field_video']['type'] = 'youtube_url';
  $handler->display->display_options['fields']['field_video']['settings'] = array(
    'link' => 0,
  );
  /* Campo: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="http://www.youtube.com/v/[field_video]?fs=1&amp;width=640&amp;height=480&amp;hl=en_US1&amp;iframe=true&amp;rel=0" class="colorbox-load">
<img src="http://img.youtube.com/vi/[field_video]/0.jpg">
</a>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $translatables['videos'] = array(
    t('Master'),
    t('Gestor de Videos'),
    t('more'),
    t('Apply'),
    t('Reiniciar'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('	<div class="container-top">
		<div class="video-title">[title]</div>                
                <div class="video-info">
                      <div class="body-value">
                            [field_video_descripcion] 
                       </div>
                 </div>
	</div>'),
    t('Page'),
    t('[field_video-video_id]'),
    t('<a href="http://www.youtube.com/v/[field_video]?fs=1&amp;width=640&amp;height=480&amp;hl=en_US1&amp;iframe=true&amp;&autoplay=1&amp;rel=0" class="colorbox-load">
<img src="http://img.youtube.com/vi/[field_video]/0.jpg">
</a>'),
    t('Block'),
    t('<a href="/videos">Ver más videos</a>'),
    t('<a href="http://www.youtube.com/v/[field_video]?fs=1&amp;width=640&amp;height=480&amp;hl=en_US1&amp;iframe=true&amp;rel=0" class="colorbox-load">
<img src="http://img.youtube.com/vi/[field_video]/0.jpg">
</a>'),
  );
  $export['videos'] = $view;

  return $export;
}
