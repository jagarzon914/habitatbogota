<!DOCTYPE html>
<html>
<head>
	<title><?php print $head_title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php print $head ?>
	<?php print $styles ?>
	<?php print $scripts ?>
</head>
<body class="<?php print $classes; ?>">
	<?php print $page_top ?>
	<?php print $page ?>
	<?php print $page_bottom ?>
</body>
</html>