jQuery(document).ready(function($) {
    var alto = $(window).height();
    var ancho = $(window).width();
    var header_alto = $('#encabezado').height();
    var nav = jQuery('#menu-principal');
    var nav2 = jQuery('#block-menu-menu-transparencia-segundo-nivel');
    var barra = jQuery('#block-barra-institucional-barra-institucional-block');

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > (100)) {
            nav.addClass("f-nav");
            nav2.addClass("f-nav2");
            barra.addClass("in");
        } else {
            nav.removeClass("f-nav");
            nav2.removeClass("f-nav2");
            barra.removeClass("in");
        }
    });
});
jQuery(document).ready(function(){
    jQuery('.collapse .fieldset-legend').click(function(){
        var a = jQuery(this);
        var el = jQuery(a.parents()[1]);
        if( el.hasClass("collapsed")){
             el.removeClass("collapsed");
        }else{
            el.addClass("collapsed");
        }
    });// este es un arreglo temporal del collapsible
    
    jQuery('.powered_by').hide();

    jQuery('#block-views-videos-home-block-1').addClass("content-tab");
    jQuery('#block-views-fotografias-block-1').addClass("content-tab");
    jQuery('#block-views-calendar-block-1').addClass("content-tab");
    jQuery('#block-block-4').addClass("content-tab");
    jQuery('#block-block-6').addClass("content-tab");
    jQuery('.content-tab').addClass("oculto");
    jQuery('#block-block-4').removeClass("oculto");

    jQuery('.front .tab').click(function(){
        jQuery('.front .tab').addClass("no-select");
        jQuery('.front .tab').removeClass("select");
        jQuery(this).removeClass("no-select");
        jQuery(this).addClass("select");

        var tab = jQuery(this);

        if (tab.attr("id")=="videos"){
            var tabs=jQuery('#block-views-videos-home-block-1');
            if( tabs.hasClass("oculto")){
                //alert(tab.attr("id"));
                jQuery('.content-tab').addClass("oculto");
                tabs.removeClass("oculto");
            }
        }
        if (tab.attr("id")=="fotos"){
            var tabs=jQuery('#block-views-fotografias-block-1');
            if( tabs.hasClass("oculto")){
                //alert(tab.attr("id"));
                jQuery('.content-tab').addClass("oculto");
                tabs.removeClass("oculto");
            }
        }
        if (tab.attr("id")=="medios"){
            var tabs=jQuery('#block-block-4');
            if( tabs.hasClass("oculto")){
                //alert(tab.attr("id"));
                jQuery('.content-tab').addClass("oculto");
                tabs.removeClass("oculto");
            }
        }        
        if (tab.attr("id")=="audios"){
             var tabs=jQuery('#block-block-6');
            if( tabs.hasClass("oculto")){
                //alert(tab.attr("id"));
                jQuery('.content-tab').addClass("oculto");
                tabs.removeClass("oculto");
            }
        }        
        if (tab.attr("id")=="agenda"){
            var tabs=jQuery('#block-views-calendar-block-1');
            if( tabs.hasClass("oculto")){
                //alert(tab.attr("id"));
                jQuery('.content-tab').addClass("oculto");
                tabs.removeClass("oculto");
            }
        }
        if (tab.attr("id")=="publicaciones"){

        }  
    });// este espara lkas tabs del home
});
jQuery(document).ready(function() {
    jQuery('.popupok .colorbox-node').attr('id', 'popup');
    jQuery("#popup").trigger("click");
});