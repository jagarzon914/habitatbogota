<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es" class="js">
<head>
	<title><?php print $head_title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php print $styles ?>
	<?php print $head ?>
	<?php print $scripts ?>
</head>
<body class="<?php print $classes; ?>">
	<?php print $page_top ?>
	<?php print $page ?>
	<?php print $page_bottom ?>
</body>
</html>