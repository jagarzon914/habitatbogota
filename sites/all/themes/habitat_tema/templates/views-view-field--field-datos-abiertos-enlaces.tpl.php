<?php
//dsm($row);
$enlaces = $row->field_field_datos_abiertos_enlaces;
?>

<table class="pure-table">
    <thead>
	<tr>
	    <th>Titulo</th>
	    <th>Enlace</th>
	</tr>
    </thead>
    <tbody>
<?php foreach ($enlaces as $enlace) :?>
    <tr>
	<td><?php print render($enlace['rendered']['#element']['title']); ?>
	</td>
	<td><?php print render($enlace['rendered']['#element']['url']); ?></td>
    </tr>
<?php endforeach; ?>

    </tbody>
</table>
