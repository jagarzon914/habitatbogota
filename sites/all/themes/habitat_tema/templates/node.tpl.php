<?php
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php print $user_picture; ?>
  
  <div class="pure-g">
    <div class="pure-u-2-3">
          <?php print render($title_prefix); ?>
          <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
          <?php print render($title_suffix); ?>
        </div>
        <div class="pure-u-1-3" style="text-align: right;"> 
          <?php if (!empty($content['links'])): ?>
            <div class="links"><?php print render($content['links']); ?></div>
          <?php endif; ?>
        </div>
  </div>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <div class="clearfix">
    <br>
    <a href="javascript:history.back(1)" class="pure-button btn-back">Volver Atrás</a>
    <?php print render($content['comments']); ?>
  </div>


</div>

